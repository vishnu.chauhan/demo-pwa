
var doCache = true; // Set this to true for production
var CACHE_NAME = "V3";

var request = self.indexedDB.open('TESTPWA', 1);
request.onsuccess = function(event) {
  var db = event.target.result;
};
request.onerror = function(event) {
 console.log('[onerror]', request.error);
};
request.onupgradeneeded = function(event) {
 var thisDB = event.target.result;
 if(!thisDB.objectStoreNames.contains("Posts")) {
   thisDB.createObjectStore("Posts",
       {autoIncrement: true});
 }
};




self.addEventListener("activate", event => {
  console.log('Service Worker: activateing');
  const cacheWhitelist = [CACHE_NAME];
  event.waitUntil(
    caches.keys().then(CACHE_NAME => {
      return Promise.all(
        CACHE_NAME.map(cache => {
          if (cache !== CACHE_NAME) {
            console.log('Service Worker: Clearing Old Cache');
            return caches.delete(cache);
          }
        })
      );
    })
  );

});


var urlsToCache = [
  '../build/index.html',
  'manifest.json',
  'index.html',
  '/about',
  '/contact',
  '/',
];

self.addEventListener("install", function(event) {
   console.log('Service Worker: Installing');
    event.waitUntil(
      caches
        .open(CACHE_NAME)
        .then(cache => {
          console.log('Service Worker: Caching Files');
          cache.addAll(urlsToCache);
        })
        .then(() => self.skipWaiting())
    );
});

self.addEventListener("fetch", function(event) {
  console.log('Service Worker: fetching');
  event.respondWith(
    fetch(event.request)
      .then(res => {
        // Make copy/clone of response
        const resClone = res.clone();
        // Open cahce
        caches.open(CACHE_NAME).then(cache => {
          // Add response to cache
          //cache.put(event.request, resClone);
        });
        return res;
      })
      .catch(err => caches.match(event.request).then(res => res))
  );
});

