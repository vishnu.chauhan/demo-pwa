import React from "react";
import { NavLink } from "react-router-dom";
import { ToastContainer } from 'react-toastify';
import { toast } from 'react-toastify';


import 'react-toastify/dist/ReactToastify.css';

class Header extends React.Component {

  render() {
    return (
    <div>
       {/* <Online onChange={this.showOnlineOfflineStats}></Online> */}
      <nav className="navbar navbar-default">
          <ToastContainer position="top-right" />
          <div className="navbar-header">
            <a className="navbar-brand"></a>
          </div>
          <ul className="nav navbar-nav pull-right">
            <li> <NavLink className="nav-link-icon"  to="/" tag={"Home"}> Home </NavLink>  </li> 
            <li> <NavLink className="nav-link-icon"  to="/about" tag={"Home"}> About </NavLink>  </li> 
            <li> <NavLink className="nav-link-icon"  to="/contact" tag={"Home"}> Contact </NavLink>  </li> 
          </ul>
      </nav>
    </div>
    );
  }
};

export default Header;
