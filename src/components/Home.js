import React from "react";
import axios from "axios";


function addPostData(db, item, collection) {
  var transaction = db.transaction([collection], "readwrite");
  var store = transaction.objectStore(collection);
  var request = store.add(item);
  request.onerror = function (e) {
    console.log("Error", e.target.error.name);
  }
  request.onsuccess = function (e) {
    console.log("added into", collection );
  }
}

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      posts:[]
    }
  }
  componentDidMount() {

  }

 async getPosts(){
    if (window.navigator.onLine == false) {
      console.log("Offline")
      var postData = [];
      var db = indexedDB.open('TESTPWA');
      db.onsuccess = function (event) {
        this.result.transaction("Posts").objectStore("Posts").getAll().onsuccess = function (event) {
          postData = event.target.result;
        }
      }
      db.onerror = function (err) {
        console.log(err);
        return false
      }
      setTimeout(() => {
        this.setState({posts:postData})
      }, 1000)
    } else {
      console.log("online")
      let result = await axios.get('https://jsonplaceholder.typicode.com/posts?_limit=10');
      console.log("result.data", result.data)
      var postData = result.data;
      postData.forEach(element => {
        var request = window.self.indexedDB.open('TESTPWA', 1);
          request.onsuccess = function (event) {
            var db = event.target.result;
            addPostData(db, element, 'Posts')
          };
          request.onerror = function (event) {
            console.log('[onerror]', request.error);
          };
      });
      this.setState({posts:result.data})
    }
  }

  getPostData(id){
    console.log("ssss", id)
  }
  render() {
    var postData = this.state.posts;
    return (
      <div className="login-page">
       <h1>Home Page</h1>
       <hr />
       <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        Why do we use it?
        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
        </p>
        <br />
        <button type="button" className="btn btn-default" onClick={(e) => { this.getPosts() }} > Get Posts </button>
        <br />
        <br />
        <br />
        <br />
        <br />
     

        {postData && postData.length > 0 && postData.map((post, index) => (
          <div key={index} className="box-item">
            <h1> {post.title} </h1>
            <p> {post.body} </p>
          </div>
        ))}

      </div>


    );
  }
};

export default Home;
