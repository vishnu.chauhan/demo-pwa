import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

import Home from "./components/Home";
import About from "./components/About";
import Contact from "./components/Contact";
import store from './store/configureStore';
//Common Components
import Footer from "./components/Common/Footer";
import Header from "./components/Common/Header";

import './App.css';

class App extends Component {
  render() {
    return (
        <Provider store={store} >
          <BrowserRouter>
            <div className="app-width">
              <Header />
              <Switch>
                <Route path="/" exact component={props => <Home {...props} />} />
                <Route path="/about" exact component={props => <About {...props} />} />
                <Route path="/contact" exact component={props => <Contact {...props} />} />
              </Switch>
              <Footer />
            </div>
          </BrowserRouter>
        </Provider>
    );
  }
}
export default App;
